#include <stdio.h>

typedef struct node {
    int val;
    struct node * next;
} node_t;

void print_list(node_t * head) {
    node_t * current = head;

    while (current != NULL) {
        printf("%d\n", current->val);
        current = current->next;
    }
}

void push(node_t ** head, int val) {
   node_t * new_node;
   new_node = malloc(sizeof(node_t));

   new_node->val = val;
   new_node->next = *head;
   *head = new_node;
 }
